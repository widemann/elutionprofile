function peaksDeconvolved = deconvolvePeaks(allPeaks, parameters)
% returns a structure with the found elution peaks grouped hopefully by
% the analyte that caused them.
numPeaks = size(allPeaks,1);
temp = cell(numPeaks,1);
maxPeakDistance = parameters.deconvolvePeaks.maxPeakDistance;
for k = 1:numPeaks
    loc = allPeaks(k,2);
    inds = find(abs(loc - allPeaks(:,2)) < maxPeakDistance);
    if numel(inds) > 1
        temp{k} = inds;
    end
end
for k = 1:numel(temp)
    n = numel(temp{k});
    if n > 0 
        for j = k+1:numel(temp)
            if n == numel(intersect(temp{k}, temp{j}))
                temp{j} = [];
            end
        end
    end
end

A = cell(0);
cnt = 1;
for k = 1:numel(temp)
    if ~isempty(temp{k})
        A{cnt} = temp{k};
        cnt = cnt + 1; 
    end
end
   
numA = numel(A);
for k = 1:numel(A)
    loc(k) = median(allPeaks(A{k},2));
end
combinedPeaks = cell(0);
cnt = 1;
for k = 1:numel(A)
    l = loc(k);
    v = find(abs(loc - l) < maxPeakDistance);
    tmp = [];
    for j = 1:numel(v)
        tmp = [tmp; A{v(j)}];
    end
    combinedPeaks{cnt} = unique(tmp);
    %loc(v) = 0;
    cnt = cnt + 1;
end
U = zeros(numel(combinedPeaks), size(allPeaks,1));
for k = 1:numel(combinedPeaks)
    U(k,combinedPeaks{k}) = 1;
end
V = unique(U,'rows');
pd = cell(0);
for k = 1:size(V,1)
    v = find(V(k,:) == 1);
    pd{k} = v;
end
         
%% get the location, peaks and mass spec signal for each peak
numPD = numel(pd);
loc = zeros(numPD,1);
for k = 1:numPD
    loc(k) = mean(allPeaks(pd{k},2));
end
[v idx] = sort(loc, 'ascend');
peaksDeconvolved = struct();
for k = 1:numPD
    peaksDeconvolved(k).loc = mean(allPeaks(pd{idx(k)},2));
    peaksDeconvolved(k).peaks = allPeaks(pd{idx(k)},:);
%     bins = allPeaks(pd{idx(k)},1); %location
%     vals = allPeaks(pd{idx(k)},3); %height
%     peaksDeconvolved(k).unitMassmass_spec = sparse(bins-parameters.zeroBin,1,vals,numel(parameters.keptBins),1);
end