function [P allPeaks parameters] = findPeaksMassBins(sampleUM, parameters)
% this function finds the peaks for each mass bin in parameters.keptBins;
% sample.time is the x-axis
% sample.S is the data 
% P is a cell with the peaks for each mass bin
% if parameters.denoiseElutionProfile % denoise the elution profile signal
%     h = daubcqf(6);
%     numTimeSamples = numel(sample.time);
%     sig = zeros(2^nextpow2(numTimeSamples),1);
%     if numTimeSamples > numel(signal)
%         error('denoising problem, time samples must be of lenght m*(2^L)')
%     end
% endallPeaks = [temp, P{k,2}]
warning off
for k = 1:numel(parameters.mz.keptBins)
    signal = full(sampleUM.S(:,k));
    m = mean(signal(parameters.time.noiseInds));
    sigma = std(signal(parameters.time.noiseInds));
    P{k,1} = parameters.mz.keptBins(k); %- parameters.zeroBin;
    parameters.findpeaks.ampThreshold = m + parameters.findpeaks.numSTDs*sigma;
    temp = fp(sampleUM.time,signal,parameters.findpeaks.SlopeThreshold,parameters.findpeaks.ampThreshold,...
                    parameters.findpeaks.smoothwidth,parameters.findpeaks.peakgroup);

    % remove nans
    if ~isempty(temp)
        inds = ~isnan(sum(temp,2));
        temp = temp(inds,2:4);   
    end
    % remove peaks that elute too early.
    if ~isempty(temp)
        inds = temp(:,1) > parameters.time.minEluter - parameters.time.timeBeforeMinEluter;
        temp = temp(inds,:);
    end
    % remove peaks that are too tall, i.e. the fit was bad. 
    maxy = 2*max(signal);
    if ~isempty(temp)
        inds = temp(:,2) < maxy;
        temp = temp(inds,:);
    end
    % remove peaks that do not have a large enough fwhm
    if ~isempty(temp)
        inds = temp(:,3) > parameters.findpeaks.elutionWidth;
        temp = temp(inds,:);
    end
    % if empty, make it "look" empty, instead of, e.g. 0x3
    if isempty(temp)
        P{k,2} = [];    
    else 
        P{k,2} = temp;
    end
%     sig(1:numTimeSamples) = ep;
%     [tmp yn opt2] = denoise(sig,h,1);
%     elution = tmp(1:numTimeSamples);
%     parameters.smoothwidth = 1;
end
warning on    

allPeaks = [];
for k = 1:size(P,1)
    if ~isempty(P{k,2})
        temp = repmat(P{k,1},size(P{k,2},1),1);
        tmp = [temp, P{k,2}];
        allPeaks = [allPeaks; tmp];
    end
end

[v idx] = sort(allPeaks(:,2));
allPeaks = allPeaks(idx,:);


