files = dir('data_dig*');
len = 30;
for k = 1:numel(files)
    tmp = load(files(k).name);
    f = fields(tmp);
    R = tmp.(f{1});
    AQ = R.AQ;
    U = R.unknowns;
    fprintf('%s\n', files(k).name)
    fprintf('name elutionTime score\n')
    for j = 1:numel(AQ)
        name = AQ{j}.name;
        for q = 1:len-length(name)
            name = [name ' '];
        end
        fprintf('%s\t %0.2f\t %0.2f\n', name, AQ{j}.loc, max(AQ{j}.combinedScore))
    end
    for q = 1:len-length('unknown')
        s = ['unknown' ' '];
    end
    for j = 1:numel(U)
        fprintf('%s\t  %0.2f\n', s, U{j}.loc)
    end
end


