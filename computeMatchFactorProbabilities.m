function p = computeMatchFactorProbabilities(mfScore, varargin)
% This function comes from: "Chemical Structure Identification by Mass
% Spectral Library Searching" Stephen E. Stein
% p(k) is the probability that amongst the lib elements found 
% mfScore.libIdx(k) is the libIndex for the kth mfScore
% mfScore.score(k) is the kth largest match factor score.

parameters.w = 75; % Not sure about this, but it's in the paper. 
if ~isempty(varargin)
    parameters = varargin;
end
p.libIdx = mfScore.libIdx;
numScores = size(mfScore.score,1);
scoresBaselined = (mfScore.score - mfScore.score(1))./parameters.w;
T = 2.^scoresBaselined;
N = sum(T);
p.prob = T./N;


