load LibCol3AC.mat
Lib = LibCol3AC;
% load the phase 2 library
load LibUniq450ml.mat
nmol2umol = 1e-3;
cnt = 1;
load elutionTransformPhase1ToPhase2.mat
warning off
for k = 1:numel(Lib.snum)
    snum = Lib.snum(k);
    %if ~ismember(snum,LibUniq450ml.Snum)
        s = full(Lib.massSpecArea(:,k));
        % find the peaks 
        sNewAxis = interp1(Lib.mz, s, LibUniq450ml.mz);
        [peaks allPeaks parameters] = completePeakAlgorithmGCMS(LibUniq450ml.mz, sNewAxis);
        if ~isempty(allPeaks)
            etOld = Lib.elutionTimes(k);
            % turn this into a phase 2 elution time 
            etNew = elutionTransformphase1Tophase2(etOld);
            ions = round(allPeaks(:,1));
            v = find(diff(ions) == 0); 
            if ~isempty(v)
                for j = numel(v):-1:1
                    inds = v(j):v(j)+1;
                    [a idx] = min(allPeaks(inds,2));
                    allPeaks(inds(idx),:) = [];
                end
            end
            ions = round(allPeaks(:,1));
            peaks = [ions, repmat(etNew,size(allPeaks,1),1), allPeaks(:,2:end)];
            temp = areaPeaks(allPeaks);
            ionsIntegrated = temp.area;
            amountOld = Lib.concScalings.data(k,5); %this is in nanomoles
            % convert to umol
            amountNew = nmol2umol*amountOld;

            phase1Tophase2Lib.Snum(cnt) = snum;
            phase1Tophase2Lib.elutionTime(cnt) = etNew;
            phase1Tophase2Lib.PrepInfo(cnt) = LibUniq450ml.PrepInfo(1);
            phase1Tophase2Lib.PrepInfo{cnt}.CompConcValues = amountNew;
            phase1Tophase2Lib.ions{cnt} = ions;
            phase1Tophase2Lib.ionsIntegrated{cnt} = ionsIntegrated;
            phase1Tophase2Lib.peaks{cnt} = peaks;
            name = Sname4Snum(snum);
            phase1Tophase2Lib.Description{cnt} = name{1};
            phase1Tophase2Lib.massSpec(:,cnt) = sNewAxis(:);
            phase1Tophase2Lib.extendedElutionWindow(cnt) = Lib.elutionWindow(k)/8;
            phase1Tophase2Lib.parameters{cnt} = 'synthetic';
            cnt = cnt + 1;
        else
            fprintf('no peaks found for %d\n',k)
        end
%     else
%         fprintf('already in Lib')
%     end
    k
end
warning on

save phase1Tophase2Lib.mat phase1Tophase2Lib