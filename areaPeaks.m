function area = areaPeaks(P)
% compute the area for each peak in P
area.peaks = P;
area.sigma = P(:,3)./(2*sqrt(2*log(2)));
area.area = P(:,2).*area.sigma.*sqrt(2*pi);
    