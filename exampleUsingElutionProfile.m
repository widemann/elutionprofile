% samples = dir('/d0/users/dwidemann/svn/ChemIST/trunk/src/matlab/SPAA/GCMS/elutionProfileScoringDeconv/*june_chal*.mat');
milli = 1e3;
% k = 1;
% s = samples(k).name;
% tic
% a = load(s);
% %             [status spectrum] = p2_get_spectrum(s,'gcms');
% %             s =strrep(s,'/','_');
% %             eval(sprintf('%s = spectrum;',s));
% %             eval(sprintf('save %s %s',[s '.mat'], s));
% toc
% %if status
% tmp = fields(a);
% spectrum = a.(tmp{1});
% tmp = spectrum.Timestamp.Value;
% sample.time = ((tmp - tmp(1))/milli)';
% sample.mz = spectrum.MZ.Value';
% sample.S = spectrum.Intensity.Value';
%% Example sample GCMS structure
% sample.time is a Tx1 vector that contains the elution time samples.
% sample.mz % is a 1xN vector that contains the mz axis
% sample.S is a TxN matrix that contains the GCMS data. 
%% Example of parameters for running the elution profile algorithm on an
%% accurate mass GCMS sample
parameters.display = 0; % display the elution peaks and the quantified data.
parameters.displayAndSaveElution = 0; % this generates a figure of the elution profiles and saves it.
if parameters.displayAndSaveElution
    parameters.saveToDirec = writeToDirec; % save the .fig to this directory. 
end
parameters.sampleVolume = 100; % set to 100 ml for now, but may change with input sample. 
% Use libGenerate = 1; if you are building a library element. It will find the
% analyte with the biggest elution profile peak and classify that as the
% library element. 
parameters.libGenerate = 0; 
% The next 4 parameters determine which mz data points to keep. 
parameters.mz.zeroBin = 25; % this is one less than the first mass bin used. 
parameters.mz.mzStart = 25.5; % use mz data points greater than this.
parameters.mz.mzEnd = 250.5; % use mz data points less than this.
parameters.mz.beforeBin = .2; % e.g. if the bin being consider is bin 53 then
parameters.mz.afterBin = .22; % the mz data points are 53-beforeBin to 53+afterBin
parameters.mz.keptBins = ceil(parameters.mz.mzStart):floor(parameters.mz.mzEnd);
parameters.mz.unitMass = 0; % is the data unit mass
% The next two parameters determine the noise indices for the data. I
% should get rid of these and just determine a ways to handle it internally.
parameters.time.minEluter = 4; %min(Lib.elutionTime); Everything that elutes before this is noise.
parameters.time.timeBeforeMinEluter = 1; % Everything that elutes before (minEluter - timeBeforeMinEluter) is noise.
% the next 4 parameters are for the peak finder.(fp for findpeaks) 
parameters.findpeaks.SlopeThreshold = 0; % the zero crossing should have a slope thresh hold of at least this.
parameters.findpeaks.smoothwidth = 5; % smooth the data with a n point boxcar filter. 
parameters.findpeaks.peakgroup = 5; % use this many points around the found zero-crossing to fit a parabola
parameters.findpeaks.numSTDs = 4; % a peak must be this many stand deviations above the mean be counted. 
parameters.findpeaks.elutionWidth = .04; % elution peak must have at least this much fwhm to be kept.
parameters.deconvolvePeaks.deconvTimeSamp = 3; % this is one of the most important paramters. It determines how many time 
% samples elution peaks have to be separated by. If a peaks are less than
% deconvTimeSamp*mean(diff(sample.time)) then they are combine into "one"
% analyte. Varying this parameter can change the algorithm results greatly.
% the next params are for unknowns, i.e. i.e. not generating a library
% element.
if ~parameters.libGenerate
    load('LibUniq450ml.mat')
    parameters.Lib = LibUniq450ml; % library to use for processing unknowns.
    parameters.scorePeaks.extendedWidth = 3; % if an analytes elutes this many seconds from the found peak, consider it. 
    % The next 5 parameters are for determining which peaks to keep in
    % peaksAssigned.
    % If the mfScore is below nPeakThresh and exactly nPeaks are found then
    % the analyte should not be estimated. This came about because there
    % are too many false positives with when less than five peaks are found
    % for an analyte. 
    parameters.assignPeaks.twoPeakThresh = 800; 
    parameters.assignPeaks.threePeakThresh = 800;
    parameters.assignPeaks.fourPeakThresh = 800;
    parameters.assignPeaks.mfScoreThresh = 800;
    parameters.assignPeaks.combineScoreThresh = .2; % this is used for every found analyte, it must have a combined
                                        % score of at least this much.
end
% the next 3 parameters are for quantification. quantifyAnalytes
parameters.quantifyAnalytes.meanFilt = 5; % smooth the signal with a boxcar of this length in order to determine which
                         % time indices should be used for quanitification.
                         % Basically, a first derivative test determines
                         % which time indices should be kept. 
parameters.quantifyAnalytes.numFwhmBefore = 5; % the data from this many fwhm's is taken before the found elution peak and smoothed.
parameters.quantifyAnalytes.numFwhmAfter = 10; % the data from this many fwhm's is taken after the found elution peak and smoothed.
parameters.combineUnknowns.combineDist = .2; % combine unknows that elute with this distance of each other. 
parameters.combineUnknowns.tooCloseToFoundAnalyte = .1; % remove any unknowns that elute within this distance of a found analyte.
     
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
                
