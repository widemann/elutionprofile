function [analytesQuantified varargout] = quantifyAnalytes(peaksAssigned, sampleUM, parameters)
% combines an analyte that was assigned to multiple peaks and then
% quantifies the results. analytesQuantified{k} is the kth analyte found. For
% quantification purposes, it's integratedMassSpec and time indices are
% most important
temp = cell(0);
numPeaks = numel(peaksAssigned);
if parameters.libGenerate 
    for k = 1:numel(peaksAssigned)
        analytesQuantified{k} = peaksAssigned{k};
        peaks = peaksAssigned{k}.peaks;
        for m = 1:size(peaks,1)
            targetIon = peaks(m,1);
            v = find(parameters.mz.keptBins == targetIon);
            if ~isempty(v)
                y = sampleUM.S(:,v);
                [integ timeInds] = quantifyPeak(sampleUM.time,y,peaks(m,2:4), parameters);
                analytesQuantified{k}.targetIon(m) = targetIon;
                analytesQuantified{k}.ionIntegrated(m) = integ;
                analytesQuantified{k}.timeInds{m} = timeInds;
            end
        end
    end
else
    %% first get the analytes that were found in the library
    Lib = parameters.Lib;
    inds = [];
    for k = 1:numPeaks
        if isfield(peaksAssigned{k}, 'assignedInd')
            inds = [inds; peaksAssigned{k}.assignedInd];
        end
    end
    cnt = 1;
    uniqueInds = unique(inds);
    for k = 1:numel(uniqueInds)
        idx = [];
        for j = 1:numPeaks
            if isfield(peaksAssigned{j}, 'assignedInd') 
                if peaksAssigned{j}.assignedInd ==  uniqueInds(k)
                    idx = [idx; j];
                end
            end
        end
        %idx = find(inds == uniqueInds(k));
        timeInds = [];
        %% do this until the target ion gets determined.
        [t tempTargetIonIdx] = max(Lib.ionsIntegrated{uniqueInds(k)});
        targetIon = Lib.ions{uniqueInds(k)}(tempTargetIonIdx);
        % check to see if it was found
        peaks = [];
        for m = 1:numel(idx)
            peaks = [peaks; peaksAssigned{idx(m)}.peaks];
        end
        if ~ismember(targetIon, peaks(:,1))
            fprintf('target ion not found...switching to new one.\n')
             [a tgIdx] = max(peaks(:,3));
             targetIon = peaks(tgIdx,1);
        end
        v = find(parameters.mz.keptBins == targetIon);
        if ~isempty(v)
            y = sampleUM.S(:,v);
            if numel(idx) > 1
                for j = 1:numel(idx)
                    peaks = peaksAssigned{idx(j)}.peaks;
                    v = find(peaks(:,1) == targetIon);
                    if ~isempty(v)
                        if numel(v) > 1
                            [pp ppIdx] = max(peaks(v,3));
                            v = v(ppIdx);
                        end
                        peak = peaks(v,2:4);
                        [integ tmp minY] = quantifyPeak(sampleUM.time,y,peak, parameters);
                        timeInds = union(timeInds, tmp);
                        minY = max(min(y(timeInds)),0);
                        %integOld = sum(y(timeInds));
                        % integ = sum(y(timeInds)-minY);
                        integ = trapz(sampleUM.time(timeInds), y(timeInds)-minY);

                    end
                end
                if ~isempty(timeInds)
                    minY = max(min(y(timeInds)),0);
                    %integOld = sum(y(timeInds));
                    % integ = sum(y(timeInds)-minY);
                    integ = trapz(sampleUM.time(timeInds), y(timeInds)-minY);
                else
                    integ = 0;
                end
            else
                peaks = peaksAssigned{idx(1)}.peaks;
                v = find(peaks(:,1) == targetIon);
                if ~isempty(v)
                    if numel(v) > 1
                        [pp ppIdx] = max(peaks(v,3));
                        v = v(ppIdx);
                    end
                    peak = peaks(v,2:4);
                    [integ timeInds minY] = quantifyPeak(sampleUM.time,y,peak, parameters);
                end
            end
            if isempty(timeInds)
                integ = 0;
            end

            temp{cnt} = peaksAssigned{idx};
            temp{cnt}.timeInds = timeInds;
            temp{cnt}.targetIon = targetIon;
            temp{cnt}.ionIntegrated = integ;
            cnt = cnt + 1;
        end
    end
end
% put them back in time order
if ~parameters.libGenerate
    numAn = numel(temp);
    analytesQuantified = cell(0);
    if numAn ~= 0
        loc = zeros(numAn,1);
        for k = 1:numAn
            loc(k) = temp{k}.loc;
        end
        [v idx] = sort(loc,'ascend');
        analytesQuantified = cell(numAn,1);
        for k = 1:numAn
            analytesQuantified{k} = temp{idx(k)};
        end
    end


    %% fit

    % fit COTs style

    for k = 1:numAn
        targetIon = analytesQuantified{k}.targetIon;
        timeInds = analytesQuantified{k}.timeInds;
        ind = analytesQuantified{k}.assignedInd;
        v = find(Lib.ions{ind} == targetIon);
        if ~isempty(v)
            libAmount = Lib.ionsIntegrated{ind}(v);
        else 
            libAmount = inf;
        end
        sampleAmount = analytesQuantified{k}.ionIntegrated;
%         sampleVolume = parameters.sampleVolume; %100; % 100 ml
%         libVolume = Lib.PrepInfo{ind}.VialVolume;
        try
            ftirAmt = Lib.FTIR{ind}.amount;
            analytesQuantified{k}.amount = (sampleAmount/libAmount)*(ftirAmt);
            %analytesQuantified{k}.amount = (sampleAmount/libAmount)*(sampleVolume/libVolume)*(ftirAmt);
        catch
            try
                analytesQuantified{k}.amount = (sampleAmount/libAmount)*(Lib.comp{ind}.val);
            catch
                analytesQuantified{k}.amount = NaN;
            end
            %analytesQuantified{k}.amount = (sampleAmount/libAmount)*(sampleVolume/libVolume)*(Lib.PrepInfo{ind}.CompConcValues);
        end   
        analytesQuantified{k}.amountUnits = 'ppm'; %Lib.PrepInfo{ind}.CompConcUnit;
        analytesQuantified{k}.name = Lib.Description{ind};
    end
end   
    %% now handle the unknowns. 
 nout = max(nargout,1)-1;
 if nout == 1 
    unknowns = cell(0);
    parameters.display = 0;
    load('quantifyingUnknowns.mat');
    unknown = cell(0);
    cnt = 1;
    for k = 1:numPeaks
        if strcmpi(peaksAssigned{k}.name, 'unknown')
            peaks = peaksAssigned{k}.peaks;
%             integSum = 0;
%             for m = 1:size(peaks,1)
%                 targetIon = peaks(m,1);
%                 y = sampleUM.S(:,targetIon - parameters.mz.zeroBin);
%                 [integ timeInds] = quantifyPeak(sampleUM.time,y,peaks(m,2:4), parameters);
%                 integSum = integSum + integ;
%             end
            area = areaPeaks(peaks(:,2:4));
            integSum = sum(area.area);
            unknown{cnt} = peaksAssigned{k};
            unknown{cnt}.integSum = integSum;
            t = peaksAssigned{k}.loc;
            idx = find(quantifyingUnknowns.timeWindows < t, 1,'last');
            if isempty(idx) 
                idx = 1;
            end
            if idx > numel(quantifyingUnknowns.a)
                idx = numel(quantifyingUnknowns.a);
            end
            p = quantifyingUnknowns.a(idx);
            unknown{cnt}.amount = integSum*p; % this assums that the function has the form outlined in quantifyingUnknowns. 
            cnt = cnt + 1;
            % compare this amount versus some of known amounts.
        end
    end
    % combine unknowns
    % remove the unknowns that are too close to found analytes. 
    unknowns = combineUnknowns(unknown, analytesQuantified, parameters);
    varargout{1} = unknowns;
end








