





for k = 1:numel(analytesQuantified)
    analytesQuantified{k}
end

for k = 1:numel(unknowns)
    unknowns{k}
end


files = dir('data_dig*unitMass.mat');
fprintf('%s\n', files(k).name)
tmp = load(files(k).name);
f = fields(tmp);
R = tmp.(f{1});
analytesQuantified = R.AQ;
len = 30;
for k = 1:numel(analytesQuantified)
    name = analytesQuantified{k}.name;
    for q = 1:len-length(name)
        name = [name ' '];
    end
    fprintf('%s\t %0.2f\t %0.2f\n', name , analytesQuantified{k}.loc, max(analytesQuantified{k}.combinedScore))
end


files = dir('data_dig*unitMass.mat');
for j = 1:numel(files)
    fprintf('%s\n', files(j).name)
    tmp = load(files(j).name);
    f = fields(tmp);
    R = tmp.(f{1});
    analytesQuantified = R.AQ;
    len = 30;
    for k = 1:numel(analytesQuantified)
        name = analytesQuantified{k}.name;
        for q = 1:len-length(name)
            name = [name ' '];
        end
    fprintf('%s\t %0.2f\t %0.2f\n', name , analytesQuantified{k}.loc, max(analytesQuantified{k}.combinedScore))
    end
    fprintf('\n')
end