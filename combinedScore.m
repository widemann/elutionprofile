function combScore = combinedScore(elutionDist,score, restrictedScore)
% this function takes the three data measurements as input and combines
% them into 1 score. The hope is that this score can be threshed to
% determine which analytes to keep and which to remove.
a = 1;
b = 1;
c = 1;
elutionDist = elutionDist(:);
score = score(:);
restrictedScore = restrictedScore(:);
f = @(x,y,z) exp(-(a*x+b*y+c*z));
N = 1000;
y = (N-score)./N;
z = (N-restrictedScore)./N;
combScore = f(elutionDist,y,z);
