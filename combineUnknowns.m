function unknown = combineUnknowns(unknowns, analytesQuantified, parameters)
% combine unknows that are close in time. 
A = unknowns;
numUn = numel(A);
d = parameters.combineUnknowns.combineDist;
cnt = 0;
temp = cell(0);
peakLocs = zeros(numel(A),1);
for j = 1:numUn
    peakLocs(j) = A{j}.loc;
end

for k = 1:numUn
    if ~isempty(A{k})
        cnt = cnt + 1;
        loc = A{k}.loc;
        integSum = 0;
        amount = 0;
        v = find(abs(peakLocs-loc) < d);
        v(v < k) = [];
        loc = mean(peakLocs(v));
        peaks = [];
        temp{cnt}.mass_spec = A{k}.mass_spec;
        for j = 1:numel(v)
            peaks = [peaks; A{v(j)}.peaks];
            integSum = integSum + A{v(j)}.integSum;
            amount = amount + A{v(j)}.amount;
            A{v(j)} = [];
        end
        temp{cnt}.loc = loc;
        temp{cnt}.peaks = peaks;
        temp{cnt}.integSum = integSum;
        temp{cnt}.amount = amount;
    end
end


%% remove if too close too a found analyte.                 
AQLocs = zeros(numel(analytesQuantified),1);
for j = 1:numel(AQLocs)
    AQLocs(j) = analytesQuantified{j}.loc;
end
d = parameters.combineUnknowns.tooCloseToFoundAnalyte;
unknown = cell(0);
cnt = 0;
for k = 1:numel(temp)
    if ~any(abs(AQLocs - temp{k}.loc) < d)
        cnt = cnt + 1;
        unknown{cnt,1} = temp{k};
    end
end
                
                
                
                
                
                