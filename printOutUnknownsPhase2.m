

samples = dir('*Unknowns.mat');
mix = cell(0);
D = [];
peaks = cell(0);
cnt = 1;
for j = 1:numel(samples)
    name = samples(j).name;
    if isempty(findstr('quantifying',name))
        fprintf('%s\n',name);
        a = load(name);
        f = fields(a);
        unknowns = a.(f{1});
        unknown = unknowns;
        for k = 1:numel(unknown)
            D = [D; unknown{k}.loc, unknown{k}.integSum, unknown{k}.amount];
            mix{cnt} = name;
            peaks{cnt} = unknown{k}.peaks;
            
            fprintf('unknown et: %0.2f\t integ: %0.4f\t amount: %0.4f\t numPeaks: %d\n',...
                unknown{k}.loc, unknown{k}.integSum, unknown{k}.amount, size(peaks{cnt},1))
            cnt = cnt + 1;
        end
    end
    fprintf('\n')
end

[v idx] = sort(D(:,1),'ascend');
nmol = 1e3;
for k = 1:numel(idx);
    name = mix{idx(k)};
    a = findstr(name,'math_mix');
    if ~isempty(a)
        name = name(a:a+8);
    else
        name = name(6:13);
    end
    fprintf('%s\t et:  %0.4f\t nmol: %d\t peak bins: ', name,v(k), round(nmol*D(idx(k),3)))
    P = peaks{idx(k)};
    locs = sort(unique(P(:,1)),'ascend');
    for j = 1:numel(locs)
        fprintf('%d\t',locs(j))
    end
    fprintf('\n');
end




