function peakScores = scorePeaks(struc, parameters)
% this function reads in the struc from the previous step
% and for each peak, it computes the forward, reverse and other associated
% scores. The librar is used to compute the scores.
% peakScores is the same as the input struc but with additional scoring
% information.
numStruc = numel(struc);
Lib = parameters.Lib;
if isfield(Lib,'HuasIons')
    library = Lib.HuasIons;
    library(library < 0) = 0;
elseif isfield(Lib,'massSpec')
    library = Lib.massSpec;
    library(library < 0) = 0;
else
    library = Lib.data;
end
for k = 1:numStruc

        dist = abs(struc(k).loc - Lib.elutionTime); 
        dist = dist - Lib.extendedElutionWindow; % for piperidine and diethylamine.
        dist(dist < 0) = 0;
        indsFd = find(dist < parameters.scorePeaks.extendedWidth); % use this and the mass spec to determine 
        if ~isempty(indsFd)
            struc(k).indsFd = indsFd;
            struc(k).elutionDist = dist(indsFd);
            posMassSpec = interp1(parameters.mz.mz, struc(k).mass_spec',Lib.mz);
            posMassSpec(posMassSpec < 0) = 0;
            mfScores = matchFactors(posMassSpec(parameters.mz.mzInds),library(parameters.mz.mzInds,indsFd));
            [v idx] = sort(mfScores.score,'descend');
            mf.libIdx = indsFd(idx);
            mf.score = v;
            struc(k).mfScore = round(mfScores.score)';
            struc(k).mfRestrictedScore = round(mfScores.restrictedScore)';
            p = computeMatchFactorProbabilities(mf);
            if parameters.scorePeaks.extendedWidth == inf
                struc(k).elutionDist = zeros(size(struc(k).elutionDist));
            end
            struc(k).combinedScore = combinedScore(struc(k).elutionDist,struc(k).mfScore,struc(k).mfRestrictedScore)';
%             a = zeros(1,numel(v));
%             a(idx) = p.prob';
%             struc(k).prob = a;
        end
end

peakScores = struc;