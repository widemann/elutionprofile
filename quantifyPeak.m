function [integ timeInds minY] = quantifyPeak(x,y,peak, parameters)
% this function computes the peak area for a peak in a signal.
% x is the x-axis.
% y is the y signal, y-axis
% peak(1) = location, peak(2) = height, peak(3) = fwhm
% parameters.meanFilt is the number of samples in the mean filter. 
% parameters.noiseInds are the indices used to compute noise statistics.
y = full(y);
f = ones(1,parameters.quantifyAnalytes.meanFilt)./parameters.quantifyAnalytes.meanFilt;
smoothedY = conv(y,f,'same');

inds = find(x > peak(1) - parameters.quantifyAnalytes.numFwhmBefore*peak(3) & x < peak(1)+ parameters.quantifyAnalytes.numFwhmAfter*peak(3));


% m = mean(y(parameters.noiseInds));
% sigma = std(y(parameters.noiseInds));
% 
% aboveSigma = y(inds) > m + sigma;

tmpInds = find(x > peak(1) - peak(3) & x <  peak(1)+ peak(3));
%[tmp idx] = min(abs(peak(1) - x(inds)));
[tmp t] = max(smoothedY(tmpInds));
idx = find(inds == tmpInds(t));
d = deriv(smoothedY(inds(1:idx))) > 0;
v = find(d == 0);
if ~isempty(v) 
    if v(end) < idx
        leftInds = inds(v(end):idx-1);
    else
        leftInds = inds(idx-1);
    end
else
    leftInds = inds(1:idx-1);
end
d = deriv(smoothedY(inds(idx:end))) < 0;
v = find(d == 0);
if ~isempty(v)
    rightInds = inds(idx:idx+v(1)-1);
else
    rightInds = inds(idx:end);
end
derivInds = [leftInds; rightInds];

timeInds = derivInds;
minY = max(min(y(timeInds)),0);
%integOld = sum(y(timeInds));
% integ = sum(y(timeInds)-minY);
integ = trapz(x(timeInds), y(timeInds)-minY);
if integ < 0 
    fprintf(2, 'integ was negative, setting it to 0.\n');
    integ = 0;
end

if parameters.display
    figure, plot(x(inds),y(inds),x(inds),smoothedY(inds)), hold on
    %plot(x(inds(aboveSigma)),y(inds(aboveSigma)), '.k')
    plot(x(derivInds),smoothedY(derivInds),'.r')
    legend('data','smoothed','derivInds')
end

