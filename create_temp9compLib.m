%% adding to the current library so that all of the 9-component mix is
%% present
% anaylytes:
% krypton  needed snum 1
% propanol needed snum 9
% diethyl ether  present idx 1 in LibUniq450ml
% cyclopentene present idx 3 in LibUniq450ml
% trichloroethene needed snum 63
% pyridine needed snum 28
% ethylbenzene present idx 6 in LibUniq450ml
% amyl acetate need snum 83
% decane present idx 7 in LibUniq450ml

% added later dichloromethane, trimethylbenzene and nitrobenzene snums 11
% 42 and 47, resp. 

load LibUniq450ml.mat
Lib = LibUniq450ml;
idx = [1 3 6 7];

for k = 1:numel(idx)
    temp.Snum(k) = Lib.Snum(idx(k));
    temp.elutionTime(k) = Lib.elutionTime(idx(k));
    temp.massSpec(:,k) = Lib.massSpec(:,idx(k));
    temp.ions{k} = Lib.ions{idx(k)};
    temp.ionsIntegrated{k} = Lib.ionsIntegrated{idx(k)};
    temp.PrepInfo{k} = Lib.PrepInfo{k};
    temp.extendedElutionWindow(k) = Lib.extendedElutionWindow(idx(k));
    temp.peaks{k} = Lib.peaks{idx(k)};
    temp.mz = Lib.mz;
    temp.Description{k} = Lib.Description{idx(k)};
    temp.FTIR{k}.amount = Lib.FTIR{idx(k)}.amount;
end

%% synthetic data
load LibCol3AC.mat
snums = [1 9 63 28 83 11 42 47];
cnt = numel(temp.Snum);
for j = 1:numel(snums)
    temp.Snum(j+cnt) = snums(j);
    v = find(LibCol3AC.snum == snums(j));
    temp.elutionTime(j+cnt) = 0;
    ms = interp1(LibCol3AC.mz, LibCol3AC.HuasIons(:,v), Lib.mz);
    ms(isnan(ms)) = 0; 
    temp.massSpec(:,j+cnt) = ms;
    temp.ions{j+cnt} = temp.ions{cnt}; % fake data
    temp.ionsIntegrated{j+cnt} = temp.ionsIntegrated{cnt}; % fake data
    temp.PrepInfo{j+cnt} = temp.PrepInfo{cnt}; % fake data
    temp.extendedElutionWindow(j+cnt) = 0;
    temp.peaks{j+cnt} = temp.peaks{cnt}; % fake data
    name = Sname4Snum(snums(j));
    temp.Description{j+cnt} = name{1};
    temp.FTIR{j+cnt}.amount = inf;
end

%% get the ions and ionsIntegrated 
for k = 1:size(temp.massSpec,2)
    peaks = findGCMSmassCalPeaks(temp.mz,temp.massSpec(:,k));
    u = unique(round(peaks(:,2)));
    temp.ions{k} = u;
    ionsInt = zeros(numel(u),1);
    for m = 1:numel(u)
        inds = find(round(peaks(:,2)) == u(m));
        ionsInt(m) = sum(peaks(inds,3));
    end
    temp.ionsIntegrated{k} = ionsInt;
end
        
temp9compLib = temp;
save temp9compLib.mat temp9compLib





