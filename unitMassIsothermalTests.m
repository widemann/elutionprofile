path = '/chemist_data/p2_raw/gcms1/';
tmp = dir(fullfile(path,'method*'));
for j = 1:numel(tmp)
    files{j} = tmp(j).name;
end
% path = '/chemist_data/bill_export/gcms_20110725/unitmass';
% files = { ...
%     'data_dig_2011-07-15_11-45-27.cdf', ...
%     'data_dig_2011-07-15_11-47-22.cdf', ...
%     'data_dig_2011-07-15_11-49-17.cdf', ...
%     'data_dig_2011-07-15_11-50-08.cdf', ...
%     'data_dig_2011-07-15_11-52-18.cdf', ...
%     'data_dig_2011-07-15_11-53-55.cdf', ...
%     'data_dig_2011-07-15_14-26-01.cdf', ...
%     };
%%
milli = 1e3;
%% set the parameters. 
parameters.display = 0; % display the elution peaks and the quantified data.
parameters.displayAndSaveElution = 0; % this generates a figure of the elution profiles and saves it.
parameters.sampleVolume = 100; % set to 100 ml for now, but may change with input sample. 
% Use libGenerate = 1; if you are building a library element. It will find the
% analyte with the biggest elution profile peak and classify that as the
% library element. 
parameters.libGenerate = 0; 
% The next 4 parameters determine which mz data points to keep. 
parameters.mz.zeroBin = 29; % this is one less than the first mass bin used. 
parameters.mz.mzStart = 29.5; % use mz data points greater than this.
parameters.mz.mzEnd = 250.5; % use mz data points less than this.
parameters.mz.beforeBin = .2; % e.g. if the bin being consider is bin 53 then
parameters.mz.afterBin = .22; % the mz data points are 53-beforeBin to 53+afterBin
parameters.mz.keptBins = ceil(parameters.mz.mzStart):floor(parameters.mz.mzEnd);
parameters.mz.unitMass = 1; % is the data unit mass
% The next two parameters determine the noise indices for the data. I
% should get rid of these and just determine a ways to handle it internally.
parameters.time.minEluter = 4; %min(Lib.elutionTime); Everything that elutes before this is noise.
parameters.time.timeBeforeMinEluter = 1; % Everything that elutes before (minEluter - timeBeforeMinEluter) is noise.
% the next 4 parameters are for the peak finder.(fp for findpeaks) 
parameters.findpeaks.SlopeThreshold = 0; % the zero crossing should have a slope thresh hold of at least this.
parameters.findpeaks.smoothwidth = 5; % smooth the data with a n point boxcar filter. 
parameters.findpeaks.peakgroup = 5; % use this many points around the found zero-crossing to fit a parabola
parameters.findpeaks.numSTDs = 4; % a peak must be this many stand deviations above the mean be counted. 
parameters.findpeaks.elutionWidth = .04; % elution peak must have at least this much fwhm to be kept.
parameters.deconvolvePeaks.deconvTimeSamp = 3; % this is one of the most important paramters. It determines how many time 
% samples elution peaks have to be separated by. If a peaks are less than
% deconvTimeSamp*mean(diff(sample.time)) then they are combine into "one"
% analyte. Varying this parameter can change the algorithm results greatly.
% the next params are for unknowns, i.e. i.e. not generating a library
% element.
if ~parameters.libGenerate
    load('unitMass9compLib.mat')
    parameters.Lib = unitMass9compLib; % library to use for processing unknowns.
    parameters.scorePeaks.extendedWidth = inf; % if an analytes elutes this many seconds from the found peak, consider it. 
    % The next 5 parameters are for determining which peaks to keep in
    % peaksAssigned.
    % If the mfScore is below nPeakThresh and exactly nPeaks are found then
    % the analyte should not be estimated. This came about because there
    % are too many false positives with when less than five peaks are found
    % for an analyte. 
    parameters.assignPeaks.twoPeakThresh = 800; 
    parameters.assignPeaks.threePeakThresh = 800;
    parameters.assignPeaks.fourPeakThresh = 800;
    parameters.assignPeaks.mfScoreThresh = 800;
    parameters.assignPeaks.combineScoreThresh = .2; % this is used for every found analyte, it must have a combined
                                        % score of at least this much.
end
% the next 3 parameters are for quantification. quantifyAnalytes
parameters.quantifyAnalytes.meanFilt = 5; % smooth the signal with a boxcar of this length in order to determine which
                         % time indices should be used for quanitification.
                         % Basically, a first derivative test determines
                         % which time indices should be kept. 
parameters.quantifyAnalytes.numFwhmBefore = 5; % the data from this many fwhm's is taken before the found elution peak and smoothed.
parameters.quantifyAnalytes.numFwhmAfter = 10; % the data from this many fwhm's is taken after the found elution peak and smoothed.
parameters.combineUnknowns.combineDist = .2; % combine unknows that elute with this distance of each other. 
parameters.combineUnknowns.tooCloseToFoundAnalyte = .1; % remove any unknowns that elute within this distance of a found analyte.
%%
for k = 1:numel(files)
    filename = fullfile(path, files{k});
    sample = readInUnitMassCDF(filename);
    S = sample.S;
    S(isnan(S) | S<0) = 0;
    sample.S = S;

    try
        tic;
        [analytesQuantified unknowns allPeaks peaksDeconvolved peakScores peaksAssigned] = elutionProfileTwice(sample,parameters);
        runTime = toc
        s = strrep(files{k}, '.cdf', '');
        ss = [strrep(s,'-','_') 'unitMass'];
        eval(sprintf('%s.AQ = analytesQuantified',ss));
        eval(sprintf('%s.unknowns = unknowns',ss));
        eval(sprintf('%s.allPeaks = allPeaks',ss));
        eval(sprintf('%s.peaksDeconvolved = peaksDeconvolved',ss));
        eval(sprintf('%s.peakScores = peakScores',ss));
        eval(sprintf('%s.peaksAssigned = peaksAssigned',ss));
        %eval(sprintf('%s.runTime = runTime',ss));
        %eval(sprintf('%s.preprocessingRunTime = preprocessingRunTime',ss));
        %eval(sprintf('%s.fileDate = fileDate',ss));
        eval(sprintf('save %s %s;', [ss '.mat'], ss));
    catch me
        f = [ss '.txt'];
        fid = fopen(f,'wt');
        fprintf(fid,me.message);
        fclose(fid);
    end
end
































