
load temp9compLib.mat
Lib = temp9compLib;
n = numel(Lib.Snum);
unitMass9compLib = Lib;
startBin = 30;
endBin = 250;
beforeBin = .2;
afterBin = .22;
unitMass9compLib.mz = startBin:endBin;
m = numel(unitMass9compLib.mz);
massSpec = zeros(m, n);
for k = 1:m
    inds = find(Lib.mz > unitMass9compLib.mz(k) - beforeBin & Lib.mz < unitMass9compLib.mz(k)+afterBin);
    massSpec(k,:) = trapz(Lib.massSpec(inds,:));
end
unitMass9compLib.massSpec = massSpec;
save unitMass9compLib.mat unitMass9compLib;