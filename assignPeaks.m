function peaksAssigned = assignPeaks(peakScores,parameters)
% this function takes peakScores and determines which analytes to assign to
% each peak. Right now, only one analyte per peak, but this may change
Lib = parameters.Lib;
numPS = numel(peakScores);
peaksAssigned = cell(numPS,1);
for k = 1:numPS
    [v idx] = max(peakScores(k).combinedScore);
    numPeaks = size(peakScores(k).peaks,1);
    score = peakScores(k).mfScore(idx);
    if (v > parameters.assignPeaks.combineScoreThresh) 
        if numPeaks > 4 
            peaksAssigned{k} = peakScores(k);
            peaksAssigned{k}.assignedInd = peakScores(k).indsFd(idx);
            peaksAssigned{k}.name = Lib.Description{peaksAssigned{k}.assignedInd}; 
        else
            % special cases when only a few peaks are detected. 
            % if only 1 peak is detected it's not kept in peaksDeconvolved
            % and hence peakScores.
            if  numPeaks == 2 && score > parameters.assignPeaks.twoPeakThresh
                peaksAssigned{k} = peakScores(k);
                peaksAssigned{k}.assignedInd = peakScores(k).indsFd(idx);
                peaksAssigned{k}.name = Lib.Description{peaksAssigned{k}.assignedInd}; 
            elseif numPeaks == 3 && score > parameters.assignPeaks.threePeakThresh
                peaksAssigned{k} = peakScores(k);
                peaksAssigned{k}.assignedInd = peakScores(k).indsFd(idx);
                peaksAssigned{k}.name = Lib.Description{peaksAssigned{k}.assignedInd}; 

            elseif numPeaks == 4 && score > parameters.assignPeaks.fourPeakThresh
                peaksAssigned{k} = peakScores(k);
                peaksAssigned{k}.assignedInd = peakScores(k).indsFd(idx);
                peaksAssigned{k}.name = Lib.Description{peaksAssigned{k}.assignedInd}; 
            else
                peaksAssigned{k} = peakScores(k);
                peaksAssigned{k}.name = 'unknown';
            end
        end
    else
        peaksAssigned{k} = peakScores(k);
        peaksAssigned{k}.name = 'unknown';
    end
end












