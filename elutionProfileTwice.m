function [analytesQuantified varargout] = elutionProfileTwice(sample,parameters)
% See elutionProfile.m for a less mangled version. The twice comes in
% because I have to do deconvolution twice. I can't think of a good way to
% do this just once. 
% Gives an estimate, analytesQuantified, of what is in the sample. The other
% output variables are used for debugging. AnalytesQuantified is a struct
% that constains an estimate for each analyte found. Read the documentation
% for a better description of it.
% varargout{1} = found unknowns in the mixture. 
% varargout{2} = allPeaks; % all the peaks found by the peak finder. It is
% an nx4 matrix containing [mass bin, elution time, peak height, fwhm]
% varargout{3} = peaksDeconvolved; % these peaks deconvolved and grouped.
% This function takes in a GCMS sample, and parameters that should 
% contain:
%% Example sample GCMS structure
% sample.time is a Tx1 vector that contains the elution time samples.
% sample.mz % is a 1xN vector that contains the mz axis
% sample.S is a TxN matrix that contains the GCMS data. 
%% Example of parameters for running the elution profile algorithm on an
% accurate mass GCMS sample
% parameters.display = 0; % display the elution peaks and the quantified data.
% parameters.sampleVolume = 100; % set to 100 ml for now, but may change with input sample. 
% % Use libGenerate = 1; if you are building a library element. It will find the
% % analyte with the biggest elution profile peak and classify that as the
% % library element. 
% parameters.libGenerate = 0; 
% % The next 4 parameters determine which mz data points to keep. 
% parameters.mz.zeroBin = 25; % this is one less than the first mass bin used. 
% parameters.mz.mzStart = 25.5; % use mz data points greater than this.
% parameters.mz.mzEnd = 250.5; % use mz data points less than this.
% parameters.mz.beforeBin = .2; % e.g. if the bin being consider is bin 53 then
% parameters.mz.afterBin = .22; % the mz data points are 53-beforeBin to 53+afterBin
% parameters.mz.keptBins = ceil(parameters.mz.mzStart):floor(parameters.mz.mzEnd);
% parameters.mz.unitMass = 0; % is the data unit mass
% % The next two parameters determine the noise indices for the data. I
% % should get rid of these and just determine a ways to handle it internally.
% parameters.time.minEluter = 4; %min(Lib.elutionTime); Everything that elutes before this is noise.
% parameters.time.timeBeforeMinEluter = 1; % Everything that elutes before (minEluter - timeBeforeMinEluter) is noise.
% % the next 4 parameters are for the peak finder.(fp for findpeaks) 
% parameters.findpeaks.SlopeThreshold = 0; % the zero crossing should have a slope thresh hold of at least this.
% parameters.findpeaks.smoothwidth = 5; % smooth the data with a n point boxcar filter. 
% parameters.findpeaks.peakgroup = 5; % use this many points around the found zero-crossing to fit a parabola
% parameters.findpeaks.numSTDs = 4; % a peak must be this many stand deviations above the mean be counted. 
% parameters.findpeaks.elutionWidth = .04; % elution peak must have at least this much fwhm to be kept.
% parameters.deconvolvePeaks.deconvTimeSamp = 3; % this is one of the most important paramters. It determines how many time 
% % samples elution peaks have to be separated by. If a peaks are less than
% % deconvTimeSamp*mean(diff(sample.time)) then they are combine into "one"
% % analyte. Varying this parameter can change the algorithm results greatly.
% % the next params are for unknowns, i.e. i.e. not generating a library
% % element.
% if ~parameters.libGenerate
%     load('LibUniq450ml.mat')
%     parameters.Lib = LibUniq450ml; % library to use for processing unknowns.
%     parameters.scorePeaks.extendedWidth = 3; % if an analytes elutes this many seconds from the found peak, consider it. 
%     % The next 5 parameters are for determining which peaks to keep in
%     % peaksAssigned.
%     % If the mfScore is below nPeakThresh and exactly nPeaks are found then
%     % the analyte should not be estimated. This came about because there
%     % are too many false positives with when less than five peaks are found
%     % for an analyte. 
%     parameters.assignPeaks.twoPeakThresh = 800; 
%     parameters.assignPeaks.threePeakThresh = 800;
%     parameters.assignPeaks.fourPeakThresh = 800;
%     parameters.assignPeaks.mfScoreThresh = 800;
%     parameters.assignPeaks.combineScoreThresh = .2; % this is used for every found analyte, it must have a combined
%                                         % score of at least this much.
% end
% % the next 3 parameters are for quantification. quantifyAnalytes
% parameters.quantifyAnalytes.meanFilt = 5; % smooth the signal with a boxcar of this length in order to determine which
%                          % time indices should be used for quanitification.
%                          % Basically, a first derivative test determines
%                          % which time indices should be kept. 
% parameters.quantifyAnalytes.numFwhmBefore = 5; % the data from this many fwhm's is taken before the found elution peak and smoothed.
% parameters.quantifyAnalytes.numFwhmAfter = 10; % the data from this many fwhm's is taken after the found elution peak and smoothed.
% parameters.combineUnknowns.combineDist = .2; % combine unknows that elute with this distance of each other. 
% parameters.combineUnknowns.tooCloseToFoundAnalyte = .1; % remove any unknowns that elute within this distance of a found analyte.
%  


%% these parameters come from the data in sample, includes this in
% elutionProfile.m
if isfield(sample,'mz')
    mzInds = []; % this builds the mz data points to keep. 
    for m = 1:numel(parameters.mz.keptBins)
        tmp = find(sample.mz > parameters.mz.keptBins(m) - parameters.mz.beforeBin &...
                    sample.mz < parameters.mz.keptBins(m) + parameters.mz.afterBin);
                mzInds = [ mzInds tmp];
                
    end
    parameters.mz.mzInds = mzInds;
    parameters.mz.mz = sample.mz;
else
    error('sample must have an mz axis')
end

if isfield(sample,'time')
    parameters.time.noiseInds = find(sample.time < (parameters.time.minEluter - parameters.time.timeBeforeMinEluter));
    parameters.deconvolvePeaks.maxPeakDistance = parameters.deconvolvePeaks.deconvTimeSamp*mean(diff(sample.time)); % this is the most important parameter. Change for unknown samples. 
else
    error('sample must have a time axis')
end
if isfield(sample,'vialVolume')  
    parameters.sampleVolume = 0; % cannot find this in spectrum right now. 
end
%% find the peaks in the elution profiles. 
sampleUM.mz = parameters.mz.keptBins; % only work on these bins. 
sampleUM.time = sample.time; % use this time axis. Currently, not uniform, this may change. 
if parameters.mz.unitMass % if the data is unit mass keep it. 
    sampleUM.S = sample.S;
else % make the data unit mass
    sampleUM.S = zeros(numel(sampleUM.time), numel(sampleUM.mz));
    for m = 1:numel(parameters.mz.keptBins)
        tmp = (sample.mz > parameters.mz.keptBins(m) - parameters.mz.beforeBin & sample.mz < parameters.mz.keptBins(m) + parameters.mz.afterBin);
        s = sum(tmp); % these are in place because trapz and sum vary greatly on small sets.
        if s > 10
            sampleUM.S(:,m) = trapz(sample.S(:,tmp),2);
        elseif s > 0 && s <=3
            sampleUM.S(:,m) = sum(sample.S(:,tmp),2);
        else
            error('No mz data points in this bin range.');
        end 
    end
end

[P allPeaks parameters] = findPeaksMassBins(sampleUM, parameters);
if parameters.displayAndSaveElution
    h = figure;
    plot(sampleUM.time, sampleUM.S);
    [p f] = fileparts(sample.file);
    ff = strrep(f,'_','');
    title(sprintf('%s Elution Profiles',ff),'fontsize',18,'fontweight','b'),
    xlabel('elution time seconds', 'fontsize',18,'fontweight','b')
    ylabel('volts','fontsize',18,'fontweight','b')
    grid on
        set(gca,...
             'linewidth',3,...
             'xcolor',[0,0,0],...
             'fontsize',18,...
             'fontname','arial');
    s = sprintf('%sElutionProfiles',f);
    ss = strrep(s,' ','');
    screensize = get(0,'screensize');
    scr = [screensize(1) screensize(2) screensize(3) - 100  screensize(4) - 100];
    set(h,'position',scr);
    saveas(gca,fullfile(parameters.saveToDirec, [ss '.fig']), 'fig');
    saveas(gca,fullfile(parameters.saveToDirec,[ss '.jpg']), 'jpg');
    close;
end

%% determine how many distincet TIC peaks there are
peaksDeconvolved = deconvolvePeaks(allPeaks, parameters);
for k = 1:numel(peaksDeconvolved)
    mask = zeros(1,numel(sample.mz));
    peaks = peaksDeconvolved(k).peaks;
    for m = 1:size(peaks,1)
        tmp = (sample.mz > peaks(m,1) - parameters.mz.beforeBin & sample.mz < peaks(m,1) + parameters.mz.afterBin);
        mask(tmp) = 1;
    end
    [tmp idx] = min(abs(peaksDeconvolved(k).loc - sample.time));
    signal = sample.S(idx,:);
    peaksDeconvolved(k).mass_spec = signal.*mask;
end
%% if generating a library element use the TIC that has the maximum sum of peak heights and only
%% quantify that analyte
if parameters.libGenerate
%     m = 0;
%     idx = 0;
%     for n = 1:numel(peaksDeconvolved)
%         t = sum(peaksDeconvolved(n).peaks(:,3))
%         if t > m
%             m = t;
%             idx = n;
%         end
%     end
    for pkDeconIdx = 1:numel(peaksDeconvolved)
        peaksAssigned{pkDeconIdx} = peaksDeconvolved(pkDeconIdx);
    end
        analytesQuantified = quantifyAnalytes(peaksAssigned, sampleUM, parameters);
else % if not generating a library element score and assign peaks. 
    peakScores = scorePeaks(peaksDeconvolved, parameters);
    peaksAssignedFirst = assignPeaks(peakScores,parameters);
    
    %% new code.
    foundIndices = [];
    for k = 1:numel(peaksAssignedFirst)
        if ~strcmpi(peaksAssignedFirst{k}.name, 'unknown')
            foundIndices = [foundIndices; peaksAssignedFirst{k}.assignedInd];
        end
    end
    foundIndices = unique(foundIndices);
   
    parameters.deconvolvePeaks.deconvTimeSamp = .7;
    parameters.deconvolvePeaks.maxPeakDistance = parameters.deconvolvePeaks.deconvTimeSamp*mean(diff(sample.time)); % this is the most important parameter. Change for unknown samples. 
    peaksDeconvolvedSecond = deconvolvePeaks(allPeaks, parameters);
    for k = 1:numel(peaksDeconvolvedSecond)
        mask = zeros(1,numel(sample.mz));
        peaks = peaksDeconvolvedSecond(k).peaks;
        for m = 1:size(peaks,1)
            tmp = (sample.mz > peaks(m,1) - parameters.mz.beforeBin & sample.mz < peaks(m,1) + parameters.mz.afterBin);
            mask(tmp) = 1;
        end
        [tmp idx] = min(abs(peaksDeconvolvedSecond(k).loc - sample.time));
        signal = sample.S(idx,:);
        peaksDeconvolvedSecond(k).mass_spec = signal.*mask;
    end
    peakScoresSecond = scorePeaks(peaksDeconvolvedSecond, parameters);
    peaksAssignedSecond = assignPeaks(peakScoresSecond,parameters);
    numPA = numel(peaksAssignedFirst);
    cnt = 1;
    for k = 1:numel(peaksAssignedSecond)
        if ~strcmpi(peaksAssignedSecond{k}.name, 'unknown')
            if ~ismember(peaksAssignedSecond{k}.assignedInd, foundIndices)
                peaksAssignedFirst{numPA+cnt} = peaksAssignedSecond{k};
                cnt = cnt + 1;
            end
        end
    end
    %%
    [analytesQuantified] = quantifyAnalytes(peaksAssignedFirst, sampleUM, parameters);
    % [analytesQuantified unknowns] = quantifyAnalytes(peaksAssignedFirst, sampleUM, parameters);
end
nout = max(nargout,1)-1;
if nout > 0
    varargout{1} = unknowns;
    if nout > 1
        varargout{2} = allPeaks;
        if nout > 2
            varargout{3} = peaksDeconvolved;
            if nout > 3
                varargout{4} = peakScores;
                if nout > 4
                    varargout{5} = peaksAssignedFirst;
                end
            end
        end
    end
end

    
    
    
    
    
    
    
    




















    
    
    
    
    
    
    