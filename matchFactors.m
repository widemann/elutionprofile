function mf = matchFactors( signal, library, varargin)
% This function comes from: "Chemical Structure Identification by Mass
% Spectral Library Searching" Stephen E. Stein
% mf.score(k) is the matchFactor score for the kth library element. The larger
% the score the better the match. 
% signal is nx1
% library is nxm, where m is the number of library elements. 
parameters.C = 1000; % scores are between 0 and 1000;
parameters.p = .5; % Not sure about this, but it's in the paper. 
if ~isempty(varargin)
    parameters = varargin;
end
signal = signal(:);
[rows cols] = size(library);
ns = sum(signal);
nL = sum(library)';
L = library.^(parameters.p);
s = signal.^(parameters.p);

score = parameters.C*(((L'*s).^(1/parameters.p))./(ns.*nL));
mf.score = score;

% restricted, could be computed much more efficiently. 
mf.restrictedScore = zeros(cols,1);
for k = 1:cols
    mask = sign(L(:,k));
    s = (signal.*mask);
    ns = sum(s);
    if ns > 0
        s = s.^(parameters.p);
        mf.restrictedScore(k) = parameters.C*(((L(:,k)'*s).^(1/parameters.p))/(ns*nL(k)));
    end
end  
    
mf.parameters = parameters;









