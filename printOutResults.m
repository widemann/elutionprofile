function status = printOutResults(writeToDirec, parameters)
% this is just a temp function for getting all the results from runBatch
% into .txt files
% e.g. printOutResults('/d0/users/dwidemann/method_6a/')
status = 0;
try 
    Lib = parameters.Lib;
    files = dir(fullfile(writeToDirec,'*.mat')); 
    D = cell(0);
    cnt = 0;
    for k = 1:numel(files)
        name = fullfile(writeToDirec,files(k).name);
        A = load(name);
        f = fields(A);
        R = A.(f{1});
        AQ = R.AQ;
        fprintf('%s\n', name)
        for j = 1:numel(AQ)
            cnt = cnt+1;
            D{cnt,1} =  files(k).name;
            D{cnt,2} = Lib.Snum(AQ{j}.assignedInd);
            D{cnt,3} = AQ{j}.loc;
            D{cnt,4} = max(AQ{j}.combinedScore);
            D{cnt, 5} = AQ{j}.ionIntegrated;
        end   
    end

    u = Lib.Snum;
    fid = fopen(fullfile(writeToDirec,'ResultsByAnalyte.txt'),'wt');
    for k = 1:numel(u)
        name = Sname4Snum(u(k));
        fprintf(fid, '%s\n', name{1});
        for j = 1:size(D,1)
            if D{j,2} == u(k);
             fprintf(fid, '%s\t %0.2f\t %0.2f\t %0.2f\n', D{j,1}, D{j,3}, D{j,4}, D{j,5});
            end
        end
        fprintf(fid, '\n');
    end
    fclose(fid);


    %% print elution times
    fid = fopen(fullfile(writeToDirec,'ResultsByElutionTimes.txt'), 'wt');
    fprintf(fid, 'filename Snums\t');
    for j = 1:numel(Lib.Snum)
        fprintf(fid, '%d\t', Lib.Snum(j));
    end
    fprintf(fid, '\n');
    for j = 1:numel(files)
        fprintf(fid, '%s\t', files(j).name);
        for k = 1:numel(Lib.Snum)
            snum = Lib.Snum(k);
            tmp = [];
            for q = 1:size(D,1)
                if D{q,2} == snum && strcmpi(D{q,1}, files(j).name)
                    tmp = D{q,3};
                end
            end
            if isempty(tmp)
                fprintf(fid,'ND\t');
            else
                fprintf(fid, '%0.2f\t', tmp);
            end
        end
        fprintf(fid, '\n');
    end
    fclose(fid);

    %% print scores
    fid = fopen(fullfile(writeToDirec,'ResultsByScore.txt'), 'wt');
    fprintf(fid, 'filename Snums\t');
    for j = 1:numel(Lib.Snum)
        fprintf(fid, '%d\t', Lib.Snum(j));
    end
    fprintf(fid, '\n');
    for j = 1:numel(files)
        fprintf(fid, '%s\t', files(j).name);
        for k = 1:numel(Lib.Snum)
            snum = Lib.Snum(k);
            tmp = [];
            for q = 1:size(D,1)
                if D{q,2} == snum && strcmpi(D{q,1}, files(j).name)
                    tmp = D{q,4};
                end
            end
            if isempty(tmp)
                fprintf(fid,'ND\t');
            else
                fprintf(fid, '%0.2f\t', tmp);
            end
        end
        fprintf(fid, '\n');
    end
    fclose(fid);      

    %% print amounts     
    fid = fopen(fullfile(writeToDirec,'ResultsByIonsIntegrated.txt'), 'wt');
    fprintf(fid, 'filename Snums\t');
    for j = 1:numel(Lib.Snum)
        fprintf(fid, '%d\t', Lib.Snum(j));
    end
    fprintf(fid, '\n');
    for j = 1:numel(files)
        fprintf(fid, '%s\t', files(j).name);
        for k = 1:numel(Lib.Snum)
            snum = Lib.Snum(k);
            tmp = [];
            for q = 1:size(D,1)
                if D{q,2} == snum && strcmpi(D{q,1}, files(j).name)
                    tmp = D{q,5};
                end
            end
            if isempty(tmp)
                fprintf(fid,'ND\t');
            else
                fprintf(fid, '%0.2f\t', tmp);
            end
        end
        fprintf(fid, '\n');
    end
    fclose(fid); 
    status = 1;
catch errorObject
    %fprintf(fid,'%s for file %s\n',msType{msCount},sampleInfo.SampleName);
    fprintf('identifier: %s\n',errorObject.identifier);
    fprintf('message:    %s\n',errorObject.message);
    eLen = length(errorObject.stack);
    for kk=1:eLen
        fprintf('level %d:\n',kk);
        fprintf('  line %d of ',errorObject.stack(kk).line);
        fprintf('%s\n',errorObject.stack(kk).name);
        fprintf('  %s\n',errorObject.stack(kk).file);
    end
end
        
        
        
        
        


