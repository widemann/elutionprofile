function sample = readInUnitMassCDF(filename, varargin)
% read in the GCMS data from a cdf file 
% e.g. sample = readInLibElement(filename,1)
% sample.mz = mz axis
% sample.time = elution times axis
% sample.S = GCMS data matrix
% sample.file = name of the cdf file containing the data.
handle = netcdf.open(filename, 'NOWRITE');
[ndims,nvars,ngatts,unlimdimid] = netcdf.inq(handle);
vars = cell(nvars,1);
for nvidx=0:nvars-1
    [varname,xtype,dimids,natts] = netcdf.inqVar(handle,nvidx);
             %fprintf('var(%d)= %s %s xtype=%d\n', nvidx, varname, strrep(value2string('dims', dimids), '\n',''),xtype);
    vars{nvidx+1} = netcdf.getVar(handle, nvidx);
    vv = vars{nvidx+1}(1);
             %fprintf('  first value = %f\n', vv);
    %size(data)
%     for naidx=0:natts-1
%         [name] = netcdf.inqAttName(handle,nvidx, naidx);
%                  fprintf('  att= %s\n', name);
%         [value] = netcdf.getAtt(handle, nvidx, name);
%                  fprintf('  %s\n', strrep(value2string('attvalue', value), '\n',''));
%         data = netcdf.getVar(handle, nvidx);
%          size(data)
%     end;
end;

n = numel(vars{2});
m = numel(vars{17});
time = vars{12}(1):vars{5}(1):vars{13}(1);
mz = reshape(vars{17},m/n,n)';
mz_start = vars{10}(1);
mz_end = vars{11}(1);
v = (mz_start:mz_end)';
temp = unique(mz);
% if size(temp,1)~=size(v,1)
%     error('Not all mz vectors are the same size. Check the data.');
% end
signal = vars{18};
S = reshape(signal,m/n,n)';
%S_sparse = sparse(S);
[i j] = find(S ~= 0);
%fprintf('Percent of nonzero elements in S = %f\n', numel(i)/m);
%         figure, imagesc([mz(1,1) mz(1,end)],[time(1) time(end)],S)
%         figure, imagesc([mz(1,1) mz(1,end)],[time(1) time(end)],S>0)

sample.mz = v;
sample.time = time';
sample.S = sparse(S);
sample.file = filename;
if ~isempty(varargin)
    figure, imagesc([sample.mz(1) sample.mz(end)],[sample.time(1) sample.time(end)],full(sample.S))
    xlabel('m/z')
    ylabel('elution time (seconds)')
    title(['GCMS for file ' filename]) 
end
netcdf.close(handle);
