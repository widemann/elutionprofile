







rpc_init;
s = 'aug_demo_chal_20110908_03_0057'
GCMS = 'GCMS1';
milli = 1e3;
[status info] = p2_get_sample_info(s)
tic;
[status spectrum] = p2_load_spectra(info.SampleData.(GCMS).SampleSpectra{1},false);
[tmp mci ] = p2_sample_compute_cal_bbz(s,GCMS);
ab = mci.MassCalParams.AB;
preProcessingRunTime = toc;
fprintf('preProcessingRunTime: %0.2f\n', preProcessingRunTime);

tmp = spectrum.Timestamp.Value;
sample.time = ((tmp - tmp(1))/milli);
sample.mz = time_to_mass_coords(spectrum.Tof.Value*(milli^2),ab)';
sample.S = spectrum.Intensity.Value';
sample.file = s;
sample.SampleName = s; 


%% figure GCMS Data

   library = 'augLib.mat';
    tmp = load(library); %'/d0/users/dwidemann/svn/ChemIST/trunk/src/matlab/SPAA/GCMS/augLib.mat');
    fdTmp = fields(tmp);
    Lib = tmp.(fdTmp{1});
    
    %% set the parameters. 
    parameters.display = 0; % display the elution peaks and the quantified data.
    parameters.displayAndSaveElution = 1; % this generates a figure of the elution profiles and saves it.
    if parameters.displayAndSaveElution
        writeToDirec = pwd;
        parameters.saveToDirec = writeToDirec; % save the .fig to this directory. 
    end
    parameters.sampleVolume = 100; % set to 100 ml for now, but may change with input sample. 
    % Use libGenerate = 1; if you are building a library element. It will find the
    % analyte with the biggest elution profile peak and classify that as the
    % library element. 
    parameters.libGenerate = 0; 
    % The next 4 parameters determine which mz data points to keep. 
    parameters.mz.zeroBin = 25; % this is one less than the first mass bin used. 
    parameters.mz.mzStart = 25.5; % use mz data points greater than this.
    parameters.mz.mzEnd = 400.5; % use mz data points less than this.
    parameters.mz.beforeBin = .2; % e.g. if the bin being consider is bin 53 then
    parameters.mz.afterBin = .22; % the mz data points are 53-beforeBin to 53+afterBin
    parameters.mz.keptBins = ceil(parameters.mz.mzStart):floor(parameters.mz.mzEnd);
    % remove bins 28 and 32
    removeBins = [28 32];
    parameters.mz.keptBins(removeBins - parameters.mz.zeroBin) = [];
    parameters.mz.unitMass = 0; % is the data unit mass
    % The next two parameters determine the noise indices for the data. I
    % should get rid of these and just determine a ways to handle it internally.
    parameters.time.minEluter = 4; %min(Lib.elutionTime); Everything that elutes before this is noise.
    parameters.time.timeBeforeMinEluter = 1; % Everything that elutes before (minEluter - timeBeforeMinEluter) is noise.
    % the next 4 parameters are for the peak finder.(fp for findpeaks) 
    parameters.findpeaks.SlopeThreshold = 0; % the zero crossing should have a slope thresh hold of at least this.
    parameters.findpeaks.smoothwidth = 5; % smooth the data with a n point boxcar filter. 
    parameters.findpeaks.peakgroup = 5; % use this many points around the found zero-crossing to fit a parabola
    parameters.findpeaks.numSTDs = 4; % a peak must be this many stand deviations above the mean be counted. 
    parameters.findpeaks.elutionWidth = .04; % elution peak must have at least this much fwhm to be kept.
    parameters.deconvolvePeaks.deconvTimeSamp = 3; % this is one of the most important paramters. It determines how many time 
    % samples elution peaks have to be separated by. If a peaks are less than
    % deconvTimeSamp*mean(diff(sample.time)) then they are combine into "one"
    % analyte. Varying this parameter can change the algorithm results greatly.
    % the next params are for unknowns, i.e. i.e. not generating a library
    % element.
    if ~parameters.libGenerate
%         library = 'LibUniq450ml.mat';
%         load(library)
        svnVersion = svnRev4func(library);
        fp = which(library);
        parameters.Lib = Lib; % library to use for processing unknowns.
        parameters.Lib.svnVersion = svnVersion;
        parameters.Lib.static = fp;
        parameters.scorePeaks.extendedWidth = 2; % if an analytes elutes this many seconds from the found peak, consider it. 
        % The next 5 parameters are for determining which peaks to keep in
        % peaksAssigned.
        % If the mfScore is below nPeakThresh and exactly nPeaks are found then
        % the analyte should not be estimated. This came about because there
        % are too many false positives with when less than five peaks are found
        % for an analyte. 
        parameters.assignPeaks.twoPeakThresh = 800; 
        parameters.assignPeaks.threePeakThresh = 800;
        parameters.assignPeaks.fourPeakThresh = 800;
        parameters.assignPeaks.mfScoreThresh = 800;
        parameters.assignPeaks.combineScoreThresh = .2; % this is used for every found analyte, it must have a combined
                                            % score of at least this much.
    end
    % the next 3 parameters are for quantification. quantifyAnalytes
    parameters.quantifyAnalytes.meanFilt = 5; % smooth the signal with a boxcar of this length in order to determine which
                             % time indices should be used for quanitification.
                             % Basically, a first derivative test determines
                             % which time indices should be kept. 
    parameters.quantifyAnalytes.numFwhmBefore = 5; % the data from this many fwhm's is taken before the found elution peak and smoothed.
    parameters.quantifyAnalytes.numFwhmAfter = 10; % the data from this many fwhm's is taken after the found elution peak and smoothed.
    parameters.combineUnknowns.combineDist = .2; % combine unknows that elute with this distance of each other. 
    parameters.combineUnknowns.tooCloseToFoundAnalyte = .1; % remove any unknowns that elute within this distance of a found analyte.

if isfield(sample,'mz')
    mzInds = []; % this builds the mz data points to keep. 
    for m = 1:numel(parameters.mz.keptBins)
        tmp = find(sample.mz > parameters.mz.keptBins(m) - parameters.mz.beforeBin &...
                    sample.mz < parameters.mz.keptBins(m) + parameters.mz.afterBin);
                mzInds = [ mzInds tmp];
                
    end
    parameters.mz.mzInds = mzInds;
    parameters.mz.mz = sample.mz;
else
    error('sample must have an mz axis')
end

if isfield(sample,'time')
    parameters.time.noiseInds = find(sample.time < (parameters.time.minEluter - parameters.time.timeBeforeMinEluter));
    parameters.deconvolvePeaks.maxPeakDistance = parameters.deconvolvePeaks.deconvTimeSamp*mean(diff(sample.time)); % this is the most important parameter. Change for unknown samples. 
else
    error('sample must have a time axis')
end
if isfield(sample,'vialVolume')  
    parameters.sampleVolume = 0; % cannot find this in spectrum right now. 
end
%% find the peaks in the elution profiles. 
sampleUM.mz = parameters.mz.keptBins; % only work on these bins. 
sampleUM.time = sample.time; % use this time axis. Currently, not uniform, this may change. 
if parameters.mz.unitMass % if the data is unit mass keep it. 
    sampleUM.S = sample.S;
else % make the data unit mass
    sampleUM.S = zeros(numel(sampleUM.time), numel(sampleUM.mz));
    for m = 1:numel(parameters.mz.keptBins)
        tmp = (sample.mz > parameters.mz.keptBins(m) - parameters.mz.beforeBin & sample.mz < parameters.mz.keptBins(m) + parameters.mz.afterBin);
        s = sum(tmp); % these are in place because trapz and sum vary greatly on small sets.
        if s > 10
            sampleUM.S(:,m) = trapz(sample.S(:,tmp),2);
        elseif s > 0 && s <=3
            sampleUM.S(:,m) = sum(sample.S(:,tmp),2);
        else
            error('No mz data points in this bin range.');
        end 
    end
end



t0 =5;
t1 = 12;
timeInds = find(sample.time > t0 & sample.time < t1);
mzInds = 1:100;
h = figure;
imagesc(sampleUM.mz(mzInds), sampleUM.time(timeInds), log(abs(sampleUM.S(timeInds,mzInds))))
title('A Subsection of GCMS Data (Log normalized for visualization)','fontsize',18,'fontweight','b')
ylabel('elution time seconds', 'fontsize',18,'fontweight','b')
xlabel('m/z','fontsize',18,'fontweight','b')
grid on
    set(gca,...
         'linewidth',3,...
         'xcolor',[0,0,0],...
         'fontsize',18,...
         'fontname','arial');
screensize = get(0,'screensize');
scr = [screensize(1) screensize(2) screensize(3) - 100  screensize(4) - 100];
set(h,'position',scr);
color bar
print -depsc GCMS_Data


%% next image 
tAnalyte = 11.45;
[v idx]  = min(abs(sample.time-tAnalyte));
mzInds = find(sample.mz < 95.5);
h = figure;
plot(sample.mz(mzInds), sample.S(idx,mzInds))
title('Mass Spec Signal for Toluene Eluting at 11.45 seconds','fontsize',18,'fontweight','b')
ylabel('volts', 'fontsize',18,'fontweight','b')
xlabel('m/z','fontsize',18,'fontweight','b')
grid on
    set(gca,...
         'linewidth',3,...
         'xcolor',[0,0,0],...
         'fontsize',18,...
         'fontname','arial');
screensize = get(0,'screensize');
scr = [screensize(1) screensize(2) screensize(3) - 100  screensize(4) - 100];
set(h,'position',scr);
set(gca, 'tickdir','out')

print -depsc massSpec

%%
 h = figure;
plot(sampleUM.time, sampleUM.S);
title('Elution Profiles by m/z Bin','fontsize',18,'fontweight','b'),
xlabel('elution time seconds', 'fontsize',18,'fontweight','b')
ylabel('volts','fontsize',18,'fontweight','b')
grid on
    set(gca,...
         'linewidth',3,...
         'xcolor',[0,0,0],...
         'fontsize',18,...
         'fontname','arial');
screensize = get(0,'screensize');
scr = [screensize(1) screensize(2) screensize(3) - 100  screensize(4) - 100];
set(h,'position',scr);
set(gca, 'tickdir','out')

print -depsc elutionProfiles



tic;
[status EPestimate] = getElutionProfileEstimate(sample);
toc


%% figure for quantification.
title('Integration Region for Quantification','fontsize',18,'fontweight','b'),
xlabel('elution time seconds', 'fontsize',18,'fontweight','b')
ylabel('volts','fontsize',18,'fontweight','b')
grid on
    set(gca,...
         'linewidth',3,...
         'xcolor',[0,0,0],...
         'fontsize',18,...
         'fontname','arial');
print -depsc quantificationRegion

%% figure for deconvolution problem

figure, plot(sampleUM.time, sampleUM.S), hold on
plot(allPeaks(:,2), allPeaks(:,3), '*r')
title('Effects of Noise and Peak Height on Determining Peak Location','fontsize',18,'fontweight','b'),
xlabel('elution time seconds', 'fontsize',18,'fontweight','b')
ylabel('volts','fontsize',18,'fontweight','b')
grid on
    set(gca,...
         'linewidth',3,...
         'xcolor',[0,0,0],...
         'fontsize',18,...
         'fontname','arial');
     set(gca, 'tickdir','out')

print -depsc peakLocationNoise


eval(sprintf('%s_EPestimate = EPestimate;', EPestimate.sample));
eval(sprintf('save %s %s;', [EPestimate.sample '_EPestimate.mat'], [EPestimate.sample '_EPestimate']));
