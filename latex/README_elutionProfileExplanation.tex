
%
\documentclass{article}%
%\documentclass{amsart}
\usepackage{amsfonts, amstext, amsmath, amssymb}
\usepackage[all]{xy}
\usepackage{graphicx}
\usepackage{a4wide}
\usepackage{hyperref}

\usepackage{epstopdf}

\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}

\begin{document}

\newtheorem{theorem}{Theorem}
\newtheorem{proposition}{Proposition}

%\bibliographystyle{plain}
\title{Elution Profile Algorithm for Gas Chromatography-Mass Spectrometry (GCMS) Data}
\author{David P. Widemann}
\date{Sept. 16, 2011}
\maketitle
%\title{The Elution Profile Algorithm for Gas Chromatography-Mass Spectrometry (GCMS) Data}
%\author[David Widemann]{David Widemann, SRI International}
%\address{SRI International, 333 Ravenswood Ave, Menlo Park, CA 94025}
%\date{Sept. 16, 2011}
%\keywords{Elution Profile, GCMS, Gas Chromatograph-Mass Spectrometry, ChemIST}
%\begin{abstract}
%First, the elution profile algorithm for analyzing accurate mass GCMS data is presented. Results for the algorithm are then shown. After the results section, there is a discussion regarding the algorithm, its formulation, its main problems and how the algorithm could be improved.   
%\end{abstract}
%\maketitle

\section{Purpose and Team}

The purpose of this document is to:
\begin{enumerate}
	\item Explain the Elution Profile (EP) algorithm. 
	\item Explain certain design choices and how the algorithm can be improved.  
\end{enumerate}

\noindent Hopefully, someone working on fast GCMS data analysis algorithms will be able to improve on this algorithm or incorporate ideas from the EP algorithm into their own algorithm. 

The development of the EP algorithm was a collaborative effort. An SRI Chemist, Jason White, built and ran the GCMS instrument collecting all of the data. Hua Lin, another SRI Chemist, worked with several commercial GCMS software packages to evaluate various techniques and approaches to analyzing GCMS data. Hua's insight into these commercial packages and input into the EP algorithm was crucial. Bill Deans, a Computer Scientist at SRI, handled the preprocessing of the GCMS data. The preprocessing of the GCMS data involved baselining the data with a median filter, correcting a delay in the high gain channel, combining high and low gain channels and performing mass calibration. The final product of the preprocessed data allowed me to create a two dimensional data matrix with one axis being elution time samples and the other axis defined by the m/z samples. These axes and the data matrix are the data inputs for the EP algorithm. The algorithm's other input is a structure of parameters on which the algorithm's logic is based. The EP algorithm is written in MATLAB. For access to the code, please email: \textit{david.widemann@sri.com} The development of the EP algorithm was funded by DARPA for the ChemIST Project at SRI International. 

\section{Problem Statement}

Given a vial containing a mixture of analytes at various concentrations, how well can a GCMS instrument detect and quantify the amount of each analyte in the mixture? The answer to this question depends on many factors: the instrument's hardware, software, the complexity of the mixture, computational resources and the amount of time allowed for analyzing the data. The EP algorithm was formulated to optimize results against computational time for the final configuration of our GCMS system. 

The final configuration of ChemIST's GCMS system collected approximately 1,500 times samples uniformly over a 30 second period. Corresponding to each time sample is an m/z axis consisting of 80,000 samples between 0 and 400. Thus, our data matrix has a size of $T \times M$ where $T = 1,500$ and $M = 80,000$. Each data point has units of volts as registered by the MCP's ion count. Figure 1 contains a subsection of a GCMS data matrix. In this image, the red pixels correspond large voltages. It is possible to see at least five analytes eluting at separate times. 


\begin{figure}[!h]
	\centering 
		\includegraphics[width= 6in,height= 4in]{GCMS_Data.eps}
		\caption{GCMS Data}
	\label{fig: GCMS Data}
\end{figure}

If we fix an elution time, we can display the mass spectrometry signal for one of these analytes. Figure 2 displays the mass spec signal for toluene eluting at 11.45 seconds with a concentration of 228.4 parts per million (PPM). 

\begin{figure}[!h]
	\centering 
		\includegraphics[width= 6in,height= 4in]{massSpec.eps}
		\caption{Mass spec for toluene eluting at 11.45 seconds}
	\label{fig: Mass spec for toluene eluting at 11.45 seconds}
\end{figure}


\section{Elution Profile Algorithm}
%\begin{equation}
%	\left\|f - f^{F}_{m} \right\|^{2}_{2} \approx m^{-\frac{1}{2}}, \hspace{1in} m \rightarrow \infty 
%	\label{eq:}
%\end{equation}

The EP algorithm attempts to detect which analytes are in the sample, classify the detected signal against a library and then quantify the amount of the detected analyte. The algorithm has 6 steps:

\begin{enumerate}
	\item Peak detection in the elution time domain for each of the collected m/z bins.
	\item Peak deconvolution, separate peaks that belong to different analytes and combine the peaks that belong to an analyte. 
	\item Scoring based on mass spec signal and elution time. 
	\item Assignment, use thresholds to determine which scores are high enough to assign a library analyte.
	\item Quantification, integrate the largest ion to determine the amount of analyte present. 
	\item Assign and quantify unknowns. 
\end{enumerate}

Each of these steps will be explained in more detail below. 

\subsection{Peak Detection}
The first step of the EP algorithm is to perform peak detection in the time domain. First, the $T \times M$ matrix is transformed into a unit mass matrix, $T \times N$, where $N$ is the number of mass bins under consideration. This transformation of making the data unit mass is done as follows: for a given mass bin, $b_i$, the data is integrated from $b_i - \alpha$ to $b_i + \beta$ at each time sample. For example, if the mass bin is $b_i = 73$, then for each time sample the data may be integrated from 72.8 to 73.22. The $\alpha$ and $\beta$ for your data may vary. This creates a matrix of size $T \times N$. 

For each mass bin, a peak detection algorithm is ran. The peak detection algorithm is based on a  \href{http://terpconnect.umd.edu/~toh/spectrum/PeakFindingandMeasurement.htm}{zero-crossings approach developed by Professor Tom O'Haver} at the University of Maryland. For details about different parameters needed to run the peak detection algorithm, see the code. The most important parameter is \textit{amplitude}. No peaks below the set amplitude threshold will be found. Our approach to setting the amplitude is to base it on the first 3 seconds of elution time data for each mass bin. Since none of our analytes eluted within this range, the mean, $m$, and standard deviation, $\sigma$, of this portion of the signal were used to created the amplitude, e.g. \textit{amplitude} = $m + n\sigma$.

The peak finding algorithm is run for each mass bin. Its output is a four dimensional array, $P$, containing the information for each peak found: peak position (in time), peak height (volts), the full-width at half-max (FWHM) of the peak and the peak's mass bin. Figure 3 contains the elution peaks for the above sample. It is possible to use this graph to see different analytes eluting.  

\begin{figure}[!h]
	\centering 
		\includegraphics[width= 6in,height= 4in]{elutionProfiles.eps}
		\caption{Elution peaks for the converted unit mass data}
	\label{fig: Elution peaks for the converted unit mass data}
\end{figure}

The problem now is to partition this collection of peaks into subsets such that each subset of peaks corresponds to a single analyte. The term partition is used loosely since it is possible for a peak to be attributed to two distinct analytes. 

\subsection{Deconvolution}

How do you determine which peaks correspond to which analyte? The basic premise here is that all the peaks corresponding to an analyte should elute at approximately the same time. There is a parameter, \textit{peakDeconvolutionDist}, that the EP algorithm uses to assign peaks to distinct analytes. Basically, if two peaks have their  locations within \textit{peakDeconvolutionDist}, they are considered to be from the same analyte. The peaks are joined in a transitive sense in that it is possible for peak A to be close to peak B and peak B to be close to peak C. This would force peak A and peak C to be combined even though they many not be located with the necessary peak deconvolution distance. The entire collection of peaks is partitioned in this manner. The output of peak deconvolution is a MATLAB cell, $D$, such that each element of the cell contains the peaks corresponding to a single analyte. 

There are many problems with this approach. 

\begin{enumerate}
	\item Often, because of noise in the data, the peak location will be off be enough remove a peak with small amplitude from its analyte. This causes the score for that analyte to be lower and the analyte can become a false negative. 
	\item Sometimes analytes elute so close in time that the \textit{peakDeconvolutionDist} parameter is unable to separate them. If the analytes cannot be separated then at best only one can be found and the other analyte becomes a false negative. 
	\item Sometimes coelution causes important mass spec peaks to be missed. If a peak is not detected, it cannot be separated. 
\end{enumerate}

Later, I will address some of these problems. 

\subsection{Scoring}
Each element of $D$ is scored against a library of known analytes. $D\left\{k \right\}$ is a struct that contains three pieces of data: $D\left\{k \right\}$.peaks, $D\left\{k \right\}$.loc, $D\left\{k \right\}$.massSpec. $D\left\{k \right\}$.peaks is the collection of peaks that were combined as one analyte. The median of these peak locations is taken and assigned to $D\left\{k \right\}$.loc. A mask is created from the mass bins of these peaks, i.e. if a peak is found in bin n, then the mask is 1 here, else 0. The mass spec signal at the time $D\left\{k \right\}$.loc is taken and masked using the found mask. This masked mass spec signal is recorded in $D\left\{k \right\}$.massSpec. 

The mass spec signal is then scored against a subset of library mass spec signals. This subset is determined by a parameter \textit{elutionWindow}, e.g. \textit{elutionWindow} $ = 2$ seconds. Any library elements that elute within \textit{elutionWindow} of the found elution location, $D\left\{k \right\}$.loc, are used in the scoring. Two scores are computed, a forward score and a reverse score. This \href{http://www.nist.gov/mml/chemical_properties/data/stephen_stein.cfm}{scoring technique was developed by Stephen Stein at NIST}. If a library element has a mass spec signal, $L_i$, then the forward score is given by: 

\begin{equation}
	F_i = \frac{\left\langle L_i, massSpec\right\rangle}{\left\|L_i\right\|_1 \left\|massSpec\right\|_1}
	\label{eq:}
\end{equation}

The reverse score, $R_i$, is the same as the above only now the mass spec signal is restricted to have support on the same set as the library element's mass spec signal. The reason for doing this is that sometimes it is not possible to remove the effects of a coeluting analyte from the mass spec signal. Restricting to only the support of the library element allows the measured signal not to be ``punished" for peaks caused by coeluters. It's possible for an analyte to have a low forward score and high reverse score. Note, before doing the above computation, it is necessary to make the data nonnegative. This is done by zero'ing out negative mass spec values. Based on the above formula, the forward score and the reverse score should always be between 0 and 1. The higher the score the better the probability of matching the analyte to a library element. 

The final score comes from the forward score, $F_i$, the reverse score, $R_i$, and the distance the analyte eluted from the library element's elution time, 

\begin{equation}
d_i = \frac{\left|elutionTime(i) - loc\right|}{elutionWindow}.
\end{equation}

The score score is between 0 and 1 and can be adjusted using weights, $w_j, j = 1,2,3$, that assign more importance to certain components than others. The score is given by

\begin{equation}
	score(i) = e^{-(w_1F_i + w_2R_i + w_3d_i)}
	\label{eq:combined scores}
\end{equation}

The scores for element of $D$ are recorded. 

\subsection{Assignment}
For each element of $D$, the maximum score is found. If this score is greater than a score threshold parameter, e.g. 0.2, then that library analyte is assigned. If not, then this element of $D$ becomes an unkown. Actually, in practice this section of the algorithm is slightly more complicated because there is some consideration as to the number of peaks found. If less than 5 peaks are found, then the forward and reverse score must be extra high in order for a library analyte to be assigned. This cuts down on false positives. 

The output of assigning library elements is that some subset of $D$ now corresponds to found analytes, $FoundAnalytes$. 

\subsection{Quantification}

For each found analyte, its quantification is a two-step process. First, the largest peak for the found analyte is integrated over its elution times. These elution times are determined using smoothing and the first derivative test. The start time of the when the analyte elutes is determined by when the smoothed elution profile starts to increase. Then, when the elution profile stops decreasing, the analyte is said to no longer elute. Figure 4 shows the computed region of integration for an analyte. Integration over this region is performed and the baseline is subtracted, call this quantity $I_k$. 

\begin{figure}[!h]
	\centering 
		\includegraphics[width= 6in,height= 4in]{quantificationRegion.eps}
		\caption{Red points give quantification domain for signal}
	\label{fig: Red points give quantification domain for signal}
\end{figure}


This integrated amount is divided by the integrated amount in the library and then multiplied by the known amount of in the library. For example, if the $k^{th}$ found analyte is the $i^{th}$ library element then the estimated amount is 

\begin{equation}
	amount(k) = \frac{I_k}{I_i}amount(i)
	\label{eq:estimate}
\end{equation}
The quantification algorithm is slightly more complicated than this. Sometimes, the main peak of an analyte is not detected for coelution reasons. It then becomes necessary to move to a secondary peak, usually the second largest peak. 
 
\subsection{Unknowns}

Often, there are found analytes for which there is no corresponding library element. These analytes are considered unknowns in the sample. It would be nice to identify and quantify them using some larger library; however, this is a difficult problem. Currently, our approach is to integrate the largest peak of the unknown analyte much as above for known analytes. Since there is no comparison to a library element it is not possible to estimate an amount using the above technique. However, one could compare the integrated peak of the unknown with the integrated peaks for the known analytes to see where it fits in the larger scheme of measured voltages and ion counts. If we assume that the amount of fragmentation is related to when the analytes elutes, it is possible to estimate the amount of an unknown using the data for known analytes that elute close enough in time. More research in this area is needed. 

\section{Design Choices and Potential Improvements}

Below are some areas in which design choices are explained and potential improvements are explored. 

\begin{enumerate}
	\item Moving to an all accurate mass algorithm to improve SNR. 
	\item More sophisticated assignment model. 
	\item Parallelization
	\item Statistical learning approach to scoring and assignment.
\end{enumerate}
I briefly describe each issue and its potential for improvement.

\subsection{From Unit Mass to Accurate mass}

Currently, the EP algorithm scores using accurate mass data; however, peak detection is done on unit mass data in the time domain. This collapsing of the data decreases the signal-to-noise ratio (SNR) for the signal. A more computational approach would be first to detect peaks in the m/z domain. This could be done by detecting mass spec peaks for each time sample of data. The list of peaks would be much longer and it is not clear how to partition the peaks corresponding to their analytes. However, there would be an improvement in SNR and the instrument should be correspondingly more sensitive. This approach was too computational for our current system; it took approximately 15 minutes to find all the peaks in the sample. However, this process is amenable to parallelization. Peak detection could be done on each time sample simultaneously. More research is needed to determine how to deconvolve the analytes from these types of peaks. 

\subsection{Assignment Model}

The current EP algorithm assigns a library analyte based solely on its score. It has been noticed that sometimes a library analyte can be assigned to two distinct elution peaks. This happens especially with isomers such as ethylbenzene and paraxylene. A more sophisticated approach would be for the algorithm to recognize that it already assigned the library analyte and instead of re-assigning it to the next peak, it could choose the next best library candidate. This approach was not implemented because it had certain side-effects with purely eluting compounds of small amounts. Basically, the SNR being low for small quantity analytes causes the algorithm to sometimes diagnose a pure eluting analyte as two compounds. More research in this area is needed. 

\subsection{Parallelization}

The current form of the EP algorithm, including preprocessing of the data, can be made to run in parallel. The preprocssing can be done simultaneously for each time sample of data. The peak detection can be done simultaneously for each mass bin. The main bottleneck is deconvolving coeluting analytes. However, even this step can subdivided into different threads and made to run faster. Once an analyte is detected in the sample, its scoring, assignment and quantification are all independent of other detected analytes. Therefore, these steps could be done in parallel too. 

\subsection{Statistical Learning}

Our current approach to scoring and assignment is crude. Basically, elution distance and inner products are used to determine scores. These scores are then thresholded using a single parameter. There are much more nuanced approaches such as Bayesian networks and naive Bayes classifiers that could improve library detections and lower false positives. One example of this is as follows, create a set of training by running various known mixtures through the instrument. Allow the algorithm to create two classes of data, found analytes and false positives. Use the statistics for these two classes to build distributions for each class. These distributions can then be used to assign probabilities of detection for future mixes. This was not tried for the EP algorithm because of development time constraints. 

\subsection{Deconvolution}

Deconvolving coeluting analytes is the main open problem for analyzing GCMS data. Our approach outlined above worked in some cases and failed in others. When deconvolution failed, it did so for several reasons:

\begin{enumerate}
	\item Necessary peaks were not detected. This suggest that deconvolution should not depend on peak detection. However, to move away from peak detection, one would need to create an elution model for the library analytes. Many library analytes had elution profiles that did not appear to be Gaussian or conform to any other known distribution. Any model would need to take these varying distributions into account. 
	\item Peak location can be greatly affected by the SNR and peak height. The main problem here is that small peaks are affected greatly by the noise. This noise causes the peak location to be off by a certain amount, sometimes too much. The deconvolution algorithm could be improved by taking noise levels and peak height into account when partitioning peaks. Figure 5 shows how determining the peak location is affected greatly by noise in the data. 
\end{enumerate}

\begin{figure}[!h]
	\centering 
		\includegraphics[width= 6in,height= 4in]{peakLocationNoise.eps}
		\caption{Effects of noise on determining peak location}
	\label{fig: Effects of noise on determining peak location}
\end{figure}


\section{References}

O'Haver, Thomas, \textit{Introduction to Signal Processing in Analytical Chemistry}, J. Chem. Educ., vol. 68 (1991) 

\noindent Stein, S., et. al., \textit{Development of Database on Gas-Chromatographic Retention Properties of Organic Compounds}, Journal of Chromatography A, vol. 1157, issue 1-2, pages 414-421 (July, 2007). 




\end{document}
