

%% set the parameters 
parameters.display = 0; % display the elution peaks and the quantified data.
parameters.elutionWidth = .04; % elution peak must have at least this much fwhm to be kept.
parameters.extendedWidth = 3; % if an analytes elutes this many seconds from the found peak, consider it. 
parameters.sampleVolume = 100; % set to 100 ml for now, but may change with input sample. 
% the next 4 parameters are for the peak finder. 
parameters.SlopeThreshold = 0; 
parameters.smoothwidth = 5; 
parameters.peakgroup = 5;
parameters.numSTDs = 4;
% The next two parameters determine the noise indices for the data. I
% should get rid of these and just determine a ways to handle it internally.
parameters.minEluter = 4; %min(Lib.elutionTime);
parameters.timeBeforeMinEluter = 1;
mzInds = [];
for m = 1:numel(parameters.keptBins)
    tmp = find(sample.mz > parameters.keptBins(m) - parameters.beforeBin &...
                sample.mz < parameters.keptBins(m) + parameters.afterBin);
            mzInds = [ mzInds tmp];
end
parameters.mzInds = mzInds;
parameters.noiseInds = find(sample.time < (parameters.minEluter - parameters.timeBeforeMinEluter));
% the next 3 params are for lib generation, i.e. they not used if
% libGenerate = 0;
parameters.libGenerate = 1;
if parameters.libGenerate
    parameters.meanFilt = 5;
    parameters.numFwhmBefore = 5;
    parameters.numFwhmAfter = 10;
else
    load('LibUniq450ml.mat')
    parameters.Lib = LibUniq450ml; % library to use for processing unknowns.
    % the next 5 parameters are for determine which peaks to keep. peaksAssigned
    parameters.twoPeakThresh = 800;
    parameters.threePeakThresh = 800;
    parameters.fourPeakThresh = 800;
    parameters.mfScoreThresh = 800;
    parameters.combineScoreThresh = .2; 
end
% The next 4 parameters determine which mz data points to keep. 
parameters.zeroBin = 25;
parameters.mzStart = 25.5; 
parameters.mzEnd = 250.5;
parameters.beforeBin = .2; 
parameters.afterBin = .22; % e.g. 52.8 to 53.22
parameters.keptBins = ceil(parameters.mzStart):floor(parameters.mzEnd);
parameters.unitMass = 1; % is the data unit mass
% the next 3 parameters are for quantification. quantifyAnalytes
parameters.meanFilt = 5;
parameters.numFwhmBefore = 5;
parameters.numFwhmAfter = 10;

%% get the phase 2 unit mass library data
load('LibUniq450ml.mat')
Lib = LibUniq450ml;
direc = '/chemist_data/p2_cdf_cache/unit';
for k = 1:numel(Lib.Snum)
    tic;
    fname = fullfile(Lib.file{k},'04_gc_sample_a_gc.cdf');
    sample = readInUnitMassCDF(fullfile(direc,fname));
     % this is the most important parameter. Change for unknown samples. 
    parameters.maxPeakDistance = 1*mean(diff(sample.time));
    analytesQuantified = elutionProfile(sample, parameters);
    phase2UnitMassLib.Snum(k) = Lib.Snum(k);
    phase2UnitMassLib.elutionTime(k) = analytesQuantified{1}.loc;
    phase2UnitMassLib.peaks{k} = analytesQuantified{1}.peaks;
    phase2UnitMassLib.massSpec(:,k) = analytesQuantified{1}.mass_spec;
    phase2UnitMassLib.ions{k} = analytesQuantified{1}.targetIon;
    phase2UnitMassLib.ionsIntegrated{k} = analytesQuantified{1}.ionIntegrated;
    phase2UnitMassLib.parameters{k}  =  parameters;
    phase2UnitMassLib.PrepInfo{k} = Lib.PrepInfo{k};
    phase2UnitMassLib.file{k} = fullfile(direc,fname);
    phase2UnitMassLib.extendedElutionWindow(k) = Lib.extendedElutionWindow(k);
    phase2UnitMassLib.mz = sample.mz;
    phase2UnitMassLib.Description{k} = Lib.Description{k};
    toc
end

save phase2UnitMassLib.mat phase2UnitMassLib;

%% use the above library to run the challenge samples. 
direc = '/chemist_data/p2_cdf_cache/unit/june_chal_20110527_01/';
folders = dir(direc);
load phase2UnitMassLib.mat
Lib = phase2UnitMassLib;
parameters.Lib = Lib;
parameters.libGenerate = 0;
parameters.twoPeakThresh = 800;
parameters.threePeakThresh = 800;
parameters.fourPeakThresh = 800;
parameters.mfScoreThresh = 800;
parameters.combineScoreThresh = .2; 
for k = 3:numel(folders)
    fname = fullfile(folders(k).name,'02_gc_sample_a_gc.cdf');
    sample = readInUnitMassCDF(fullfile(direc,fname));
    parameters.maxPeakDistance = 3*mean(diff(sample.time));
    tic;
    analytesQuantified = elutionProfile(sample,parameters);
    toc
    eval(sprintf('%sUnitMassAQ = analytesQuantified;',folders(k).name))
    eval(sprintf('save %s %s',[folders(k).name 'UnitMassAQ.mat'], [folders(k).name 'UnitMassAQ']))
end

%% print out the results
warning 'off'
files = dir( '*AQ.mat');
for k = 1:numel(files)
    name = files(k).name;
    v = findstr(name, 'mix');
    mathMix = name(v:v+3);
    a = load(name);
    field = fields(a);
    fprintf('%s\n',name)
    name2 = strrep(name,'UnitMass','');
    printAQ(a.(field{1}), Lib, name2)
    fprintf('_____________________________\n')
end
warning 'on'




























